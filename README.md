# Shooter

This project is a part of a tutorial series Unreal 5.0 C++ Developer: Learn C++ and Make Video Games from GameDev.tv which worked as an comprehensive and structured approach to learn about unreal further. 

Topics covered:
- [x] Player movement
- [x] Animation
- [x] Damage system
- [x] Enemy AI
- [x] Simple VFX
- [x] Basic game loop


Developed with Unreal Engine 5
